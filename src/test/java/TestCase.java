
import org.example.Deliveries;
import org.example.Matches;
import org.example.Main;

import org.junit.jupiter.api.Test;

import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

public class TestCase {
    @Test
    public void testMatchesPerYear() {
        List<Matches> matches = getTestMatchesData();
        Map<Integer, Integer> actualResult = Main.matchesPerYear(matches);

        assertEquals(Integer.valueOf(3),actualResult.get(2016));
        assertEquals(Integer.valueOf(1),actualResult.get(2015));
    }
    @Test
    public void testMatchesWonPerTeam(){
        List<Matches> matches = getTestMatchesData();
        Map<String, Integer> actualResult = Main.matchesWonPerTeam(matches);

        assertEquals(Integer.valueOf(2),actualResult.get("Kolkata Knight Riders"));
        assertEquals(Integer.valueOf(2),actualResult.get("Chennai Super Kings"));
    }
    @Test
    public void testExtraRunsPerTeam(){
        List<Matches> matches = getTestMatchesData();
        List<Deliveries> deliveries = getTestDeliveriesData();
        Map<String, Integer> actualResult = Main.extraRunsPerTeam(matches, deliveries, 2016);

        assertEquals(Integer.valueOf(6), actualResult.get("Kolkata Knight Riders"));
    }

    @Test
    public void testTopEconomicalBowlers(){
        List<Matches> matches = getTestMatchesData();
        List<Deliveries> deliveries = getTestDeliveriesData();
        List<String> actualResult = Main.topEconomicalBowlers(matches, deliveries, 2015);

        assertTrue(actualResult.contains("L Balaji"));
    }
    private List<Matches> getTestMatchesData() {
        Matches match1 = new Matches();
        match1.setSeason(2016);
        match1.setId(1);
        match1.setWinner("Kolkata Knight Riders");
        match1.setVenue("Eden Gardens");

        Matches match2 = new Matches();
        match2.setSeason(2016);
        match2.setId(2);
        match2.setWinner("Kolkata Knight Riders");
        match2.setVenue("Eden Gardens");

        Matches match3 = new Matches();
        match3.setSeason(2016);
        match3.setId(3);
        match3.setWinner("Chennai Super Kings");
        match3.setVenue("Eden Gardens");

        Matches match4 = new Matches();
        match4.setSeason(2015);
        match4.setId(4);
        match4.setWinner("Chennai Super Kings");
        match4.setVenue("Eden Gardens");

        return Arrays.asList(match1, match2, match3 , match4);
    }

    private List<Deliveries> getTestDeliveriesData(){
        Deliveries delivery1 = new Deliveries();
        delivery1.setMatchId(1);
        delivery1.setBowler("SP Narine");
        delivery1.setExtraRuns(4);
        delivery1.setTotalRuns(4);
        delivery1.setBowlingTeam("Kolkata Knight Riders");

        Deliveries delivery2 = new Deliveries();
        delivery2.setMatchId(1);
        delivery2.setBowler("SP Narine");
        delivery2.setExtraRuns(0);
        delivery2.setTotalRuns(2);
        delivery2.setBowlingTeam("Kolkata Knight Riders");

        Deliveries delivery3 = new Deliveries();
        delivery3.setMatchId(1);
        delivery3.setBowler("SP Narine");
        delivery3.setExtraRuns(1);
        delivery3.setTotalRuns(1);
        delivery3.setBowlingTeam("Kolkata Knight Riders");

        Deliveries delivery4 = new Deliveries();
        delivery4.setMatchId(1);
        delivery4.setBowler("SP Narine");
        delivery4.setExtraRuns(1);
        delivery4.setTotalRuns(1);
        delivery4.setBowlingTeam("Kolkata Knight Riders");

        Deliveries delivery5 = new Deliveries();
        delivery5.setMatchId(4);
        delivery5.setBowler("L Balaji");
        delivery5.setExtraRuns(0);
        delivery5.setTotalRuns(1);
        delivery5.setBowlingTeam("Chennai Super Kings");

        return Arrays.asList(delivery1, delivery2, delivery3, delivery4, delivery5);

    }


}
