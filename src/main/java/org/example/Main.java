package org.example;

import java.io.FileNotFoundException;
import java.sql.SQLOutput;
import  java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    private static int Id_Index = 0;
    private static int Season_Index = 1;
    private static int City_Index = 2;
    private static int Date_Index = 3;
    private static int Team1_Index = 4;
    private static int Team2_Index = 5;
    private static int TossWinner_Index = 6;
    private static int TossDecision_Index = 7;
    private static int Result_Index = 8;
    private static int DlApplied_Index = 9;
    private static int Winner_Index = 10;
    private static int WinByRuns_Index = 11;
    private static int WinByWickets_Index = 12;
    private static int PlayerOfThematch_Index = 13;
    private static int Venue_Index = 14;



    private static int MatchId_Index = 0;
    private static int Inning_Index = 1;
    private static int BattingTeam_Index = 2;
    private static int BowlingTeam_Index = 3;
    private static int Over_Index = 4;
    private static int Ball_Index = 5;
    private static int Batsman_Index = 6;
    private static int NonStriker_Index = 7;
    private static int Bowler_Index = 8;
    private static int IsSuperOver_Index = 9;
    private static int WideRuns_Index = 10;
    private static int ByeRuns_Index = 11;
    private static int LegByeRuns_Index = 12;
    private static int NoBallRuns_Index = 13;
    private static int PenaltyRuns_Index = 14;
    private static int BatsmanRuns_Index = 15;
    private static int ExtraRuns_Index = 16;
    private static int TotalRuns_Index = 17;



    public static void main(String[] args) {
        String MatchesData = "/home/rudy/Public/Java MB/matches.csv";
        String DeliveriesData = "/home/rudy/Public/Java MB/deliveries.csv";


        List<Matches> matchesList = ReadMatchesData(MatchesData);
//        System.out.println(matchesList);
        List<Deliveries> deliveriesList = ReadDeliveriesData(DeliveriesData);
//        System.out.println(deliveriesList);


        Map<Integer , Integer> quesFirst = matchesPerYear(matchesList);
        System.out.println(quesFirst);

        Map<String , Integer> quesSecond = matchesWonPerTeam(matchesList);
        System.out.println(quesSecond);

//        System.out.println(getIdForTheYear(matchesList , 2015));

        Map<String , Integer> quesThird = extraRunsPerTeam(matchesList , deliveriesList , 2016);
        System.out.println(quesThird);

        List<String> quesFourth = topEconomicalBowlers(matchesList , deliveriesList , 2015);
        System.out.println(quesFourth);

    }
    public static List<Matches> ReadMatchesData(String MatchesData){
        List<Matches> matchData= new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(MatchesData))) {
            String heading ;
            boolean headRow = true;
            while ((heading = br.readLine())!= null){
                if(headRow){
                    headRow = false;
                    continue;
                }
                String[] colName = heading.split(",");
                Matches matches = new Matches();
                matches.setId(Integer.parseInt(colName[Id_Index]));
                matches.setSeason(Integer.parseInt(colName[Season_Index]));
                matches.setCity(colName[City_Index]);
                matches.setDate(colName[Date_Index]);
                matches.setTeam1(colName[Team1_Index]);
                matches.setTeam2(colName[Team2_Index]);
                matches.setTossWinner(colName[TossWinner_Index]);
                matches.setTossDecision(colName[TossDecision_Index]);
                matches.setResult(colName[Result_Index]);
                matches.setDlApplied(Integer.parseInt(colName[DlApplied_Index]));
                matches.setWinner(colName[Winner_Index]);
                matches.setWinByRuns(Integer.parseInt(colName[WinByRuns_Index]));
                matches.setWinByWickets(Integer.parseInt(colName[WinByWickets_Index]));
                matches.setPlayerOfTheMatch(colName[PlayerOfThematch_Index]);
                matches.setVenue(colName[Venue_Index]);

                matchData.add(matches);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return matchData;
    }

    public static List<Deliveries> ReadDeliveriesData(String DeliveriesData){
        List<Deliveries> deliveriesdata = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(DeliveriesData))){
            String heading;
            boolean headrow = true;
            while((heading = br.readLine())!= null){
                if(headrow){
                    headrow = false;
                    continue;
                }
                Deliveries delivery = new Deliveries();
                String[] colName = heading.split(",");
                delivery.setMatchId(Integer.parseInt(colName[MatchId_Index]));
                delivery.setInning(Integer.parseInt(colName[Inning_Index]));
                delivery.setBattingTeam(colName[BattingTeam_Index]);
                delivery.setBowlingTeam(colName[BowlingTeam_Index]);
                delivery.setOver(Integer.parseInt(colName[Over_Index]));
                delivery.setBall(Integer.parseInt(colName[Ball_Index]));
                delivery.setBatsman(colName[Batsman_Index]);
                delivery.setNonStriker(colName[NonStriker_Index]);
                delivery.setBowler(colName[Bowler_Index]);
                delivery.setIsSuperOver(Integer.parseInt(colName[IsSuperOver_Index]));
                delivery.setWideRuns(Integer.parseInt(colName[WideRuns_Index]));
                delivery.setByeRuns(Integer.parseInt(colName[ByeRuns_Index]));
                delivery.setLegByeRuns(Integer.parseInt(colName[LegByeRuns_Index]));
                delivery.setNoBallRuns(Integer.parseInt(colName[NoBallRuns_Index]));
                delivery.setPenaltyRuns(Integer.parseInt(colName[PenaltyRuns_Index]));
                delivery.setBatsmanRuns(Integer.parseInt(colName[BatsmanRuns_Index]));
                delivery.setExtraRuns(Integer.parseInt(colName[ExtraRuns_Index]));
                delivery.setTotalRuns(Integer.parseInt(colName[TotalRuns_Index]));

                deliveriesdata.add(delivery);

            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return deliveriesdata;
    }


    public static Map<Integer,Integer> matchesPerYear(List<Matches> matchesList){
        Map<Integer, Integer> matchCount = new TreeMap<>();

        for(Matches matches: matchesList){
//            System.out.println(matches);
            int year = matches.getSeason();
            matchCount.put(year , matchCount.getOrDefault(year , 0)+1);
        }
        return matchCount;
    }

    public static Map<String , Integer> matchesWonPerTeam(List<Matches> matchesList){
        Map<String , Integer> matchWonCount = new TreeMap<>();

        for(Matches matches : matchesList){
            String winner = matches.getWinner();
            if(!winner.isEmpty()){
                matchWonCount.put(winner , matchWonCount.getOrDefault(winner , 0)+1);
            }
        }
        return matchWonCount;
    }

    public static Set<Integer> getIdForTheYear(List<Matches> matchesList , int year){
        Set<Integer> ids = new HashSet<>();
        for(Matches matches : matchesList){
            if(matches.getSeason() == year){
                ids.add(matches.getId());
            }
        }
        return ids;
    }

    public static Map<String, Integer> extraRunsPerTeam(List<Matches> matchesList, List<Deliveries> deliveriesList , int year){
        Map<String,Integer> extraRunsPerTeamInYear = new TreeMap<>();
        Set<Integer> getId = getIdForTheYear(matchesList , year);

        for(Deliveries delivery : deliveriesList){
            if(getId.contains(delivery.getMatchId())){
                String teamName = delivery.getBowlingTeam();
                int extras = delivery.getExtraRuns();
                extraRunsPerTeamInYear.put(teamName , extraRunsPerTeamInYear.getOrDefault(teamName , 0) + extras);
            }
        }
        return extraRunsPerTeamInYear;
    }

    public static List<String> topEconomicalBowlers(List<Matches> matchesList , List<Deliveries> deliveriesList , int year){
        Map<String,Integer> runsConceded = new HashMap<>();
        Map<String,Integer> ballsBowled = new HashMap<>();
        Set<Integer> getId = getIdForTheYear(matchesList , year);
        for(Deliveries delivery : deliveriesList){
            if(getId.contains(delivery.getMatchId())){
                String bowlerName = delivery.getBowler();
                int runs = delivery.getTotalRuns();
                runsConceded.put(bowlerName , runsConceded.getOrDefault(bowlerName , 0) + runs);
                ballsBowled.put(bowlerName , ballsBowled.getOrDefault(bowlerName , 0) + 1);
            }
        }
            Map<String,Double> bowlerEconomy = new HashMap<>();
        for(Map.Entry<String , Integer> entry : runsConceded.entrySet()){
            int runs = runsConceded.get(entry.getKey());
            int balls = ballsBowled.get(entry.getKey());
            double economy = (double) runs/((double) balls /6);
            bowlerEconomy.put(entry.getKey() , economy );
        }

//        System.out.println(bowlerEconomy);

        List<Map.Entry<String, Double>> bowlerList = new ArrayList<>(bowlerEconomy.entrySet());
        bowlerList.sort(Map.Entry.comparingByValue());

        List<String> topBowlerList = new ArrayList<>();
        for (Map.Entry<String, Double> entry : bowlerList) {
            topBowlerList.add(entry.getKey());
        }

        List<String> limitedTopBowlerList = new ArrayList<>();
        int limit = Math.min(10, topBowlerList.size());

        for (int i = 0; i < limit; i++) {
            limitedTopBowlerList.add(topBowlerList.get(i));
        }
        return limitedTopBowlerList;
//        return topBowlerList.subList(0, Math.min(10, topBowlerList.size()));
    }
}